GPU cloud rendering toolbox
===========================

This repository is meant to give various tools and documentation to allow for using either GPU-capable instances on AWS, Microsft Azure or the likes, or a dedicated physical headless server with a GPU to run OpenGL software distantly.


## Structure of the repository

The main directories are organized like this:

- doc : documentation related to headless OpenGL rendering
- platforms : plateform-specific scripts and documentation for installing headless rendering servers, and running the software
- module: Python module retrieving Shmdatas and sending them to distant hosts

## License

All the scripts are licensed under the GNU General Public License (GPL), a copy can be found [here](./LICENSE.md).

## Simple example of how to create and send Splash created shmdatas

1. Create a GPU-capable instance on [AWS](./plateforms/AWS/README.md) or [Azure](./plateforms/Azure/README.md). Configure it using the relevant scripts for each plateform. The scripts will also download by default [Shmdata](https://gitlab.com/sat-metalab/shmdata), [Switcher](https://gitlab.com/sat-metalab/switcher) and [Splash](https://gitlab.com/sat-metalab/splash).
2. On your computer, download and install [TurboVNC](https://sourceforge.net/projects/turbovnc/files/). Run your VNC viewer and launch Splash using `vglrun splash`.
3. Create a `shmdata` in Splash:
	1. Open the *Splash Control Panel* by clicking on `Ctrl + Shift`.
	2. Add object `sink_shmdata` to the graph. Select the `win1_cam1_wrap` and link it to the `sink_shmdata` you just created by using `Shift + click`. This will create an intermediate filter object.
	3. Select the `sink_shmdata` and change its *opened* parameter from "0" to "1".
	4. The shmdata's socket name will be *splash_sink* by default. You can change it or leave it as it is. You can verify that the shmdata is actually working by running the command `sdflow /tmp/splash_sink`. You should see the frames stream.
4. Run the [shm_receiver](./module/README.md) script on the receiving server and the [shm_caller](./module/README.md) script on the calling server in which Splash is running and the shmdata is created. After a couple of minutes, a window will open on the receiving server showing the Splash user interface. As of this example, the shmdata created is named */tmp/splash_sink* and is received as */tmp/switcher_sipreceiv_receiver-sat_rtp-nvenc0*. You can check the frames received with the command `sdfow /tmp/switcher_sipreceiv_receiver-sat_nvenc0-video`. You can create and send multiple shmdatas connected to different cameras: they would appear on the receiving server on different windows. 
