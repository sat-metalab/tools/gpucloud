GPU cloud rendering using Azure
==============================

## Create a GPU Azure Server

To create a GPU Azure Server, clone this repository into a Ubuntu machine and run the script **create_azure_server.sh**. Multiple parameters need to be passed to the script:

* Your Microsoft email passed with the flag `-e` or `--email`
* Your Microsoft's account password passed with the flag `-p` or `--password`
* The new virtual machine's name passed with the flag `-n` or `--name`
* The new virtual machine's password passed with the flag `-a` or `-azure-vm-password`

Thus, the script can be run using the following command `sh ./configure_azure_server.sh -e username@microsoft-domain.com -p email-password -n new_vm-name -a new-vm-password`.

This script will create a GPU virtual machine that will be automatically running by default. Il will also show its public IP. Connect to it through SSH, for example if the Microsoft email is `somemail@hey.microsoft.com`:

```bash
ssh somemail@$IP_ADDRESS
```

You may also need to change the image hardcoded in the script, as the available images change overe time. To get a list of available Ubuntu images, use the following command line:

```bash
az vm image list --publisher Canonical
```


## Configure Azure server

Clone this repository into the Azure server and run the script **configure_azure_server.sh** using the command `sh ./configure_azure_server.sh`. It will download and install all necessary dependencies and modules, including VirtualVG and TurboVNC as well as Splash as a default software.

**Note**: if asked for, it is important to select `lightdm` as the display manager. `gdm3` is not capable of giving access to the GPU through VirtualGL.

The script will ask for rebooting the server to complete installation.


## Launch TurboVNC

On your server, run the command `sudo /opt/TurboVNC/bin/vncserver` to launch TurboVNC. It will wask for a password on the first launch. The password should contain 8 characters.


## Connect to the VNC server

On your client, connect to the port 5901 of your Azure server where TurboVNC is running using the command `ssh -L 5901:127.0.0.1:5901 *account_name@*ip_address* `

Run a VNC viewer and choose *localhost:1* as the VNC server. Enter the password you entered during the first launch. For a better experience compression should be enabled. For example when using `tightvncviewer` the following command gives good results:

```bash
vncviewer -encodings "tight copyrect" -compresslevel 9 localhost:1
```

Run Splash using the following command `vglrun splash`.
