#!/bin/bash

update_source()
{
    sudo apt update
}

install_dev_tools()
{
    echo "INSTALL DEV TOOLS"
    sudo apt-get -y install build-essential
    sudo apt-get -y install autoconf automake gdb git libffi-dev zlib1g-dev libssl-dev
}

install_xorg()
{
    echo "INSTALL XORG"
    sudo apt-get install -y xorg-*
}

install_kernel_dev()
{
    echo "INSTALL KERNEL-DEV"
    sudo apt-get install linux-headers-$(uname -r)
    sudo apt-get upgrade -y
}

install_python()
{
    echo "INSTALL PYTHON"
    sudo apt install -y python3-pip
}

install_lsb()
{
    echo "INSTALL LSB"
    sudo apt-get install -y lsb-core
}

install_awsclii()
{
    echo "INSTALL AWSCLII"
    sudo pip3 install awscli
}

install_lightdm()
{
    echo "INSTALL LIGHTDM"
    DEBIAN_FRONTEND=noninteractive sudo apt install -y lightdm
    sudo dpkg-reconfigure lightdm
    sudo apt install -y konsole
}

install_nvidia()
{
    echo "INSTALL NVIDIA"
    sudo apt install -y nvidia-driver-440 nvidia-dkms-440 nvidia-kernel-common-440 nvidia-kernel-source-440 nvidia-utils-440 nvidia-settings
    sudo systemctl set-default graphical.target
    sudo nvidia-xconfig
}

install_virtualgl()
{
    echo "INSTALL VIRTUALGL"
    sudo apt-get install -y libglu1-mesa
    wget https://sourceforge.net/projects/virtualgl/files/2.6.3/virtualgl_2.6.3_amd64.deb
    sudo dpkg -i virtualgl_*.deb
    sudo service lightdm stop
    sudo /opt/VirtualGL/bin/vglserver_config -config -s -f -t
    sudo usermod -a -G vglusers $USER
}

install_turbovnc()
{
    echo "INSTALL TURBOVNC"
    wget https://sourceforge.net/projects/turbovnc/files/2.2.5/turbovnc_2.2.5_amd64.deb
    sudo dpkg -i turbovnc_*.deb
}

install_shmdata()
{
    echo "INSTALL SHMDATA"
    cd $HOME
    sudo apt -y install cmake bison flex libtool libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev python3-dev zip
    git clone https://gitlab.com/sat-metalab/shmdata.git
    cd shmdata
    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Release .. 
    make -j$(nproc)
    sudo make install
    sudo ldconfig
}

install_splash()
{
    echo "INSTALL SPLASH"
    cd $HOME
    sudo apt -y install git-core libxrandr-dev libxi-dev \
        mesa-common-dev libgsl0-dev libatlas3-base libgphoto2-dev libz-dev \
        libxinerama-dev libxcursor-dev yasm portaudio19-dev \
        python3-numpy libopencv-dev libjsoncpp-dev libx264-dev \
        libx265-dev

    git clone https://gitlab.com/sat-metalab/splash
    cd splash
    git checkout develop
    git submodule update --init
    ./make_deps.sh
    mkdir -p build && cd build
    cmake ..
    make -j$(nproc) && sudo make install
}

install_switcher()
{
    echo "INSTALL SWITCHER"
    cd $HOME
    sudo apt -y install nvidia-cuda-toolkit nvidia-cuda-dev
    DEBIAN_FRONTEND=noninteractive sudo apt install -y bison flex libtool libglib2.0-dev libgstreamer1.0-dev \
        libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev libjson-glib-dev libcgsi-gsoap-dev \
        gstreamer1.0-libav gstreamer1.0-plugins-bad gstreamer1.0-plugins-base gstreamer1.0-plugins-good \
        gstreamer1.0-plugins-ugly liblo-dev linux-libc-dev libpulse-dev libportmidi-dev libjack-jackd2-dev \
        jackd libvncserver-dev uuid-dev libssl-dev swh-plugins  libgl1-mesa-dev libglu1-mesa-dev freeglut3-dev \
        libltc-dev libcurl4-gnutls-dev gsoap wah-plugins libsamplerate0-dev  libsoup2.4-dev libxxf86vm-dev
    git clone https://gitlab.com/sat-metalab/switcher.git
    cd switcher
    git checkout develop
    git submodule update --init --recursive
    mkdir -p build && cd build
    cmake .. -DENABLE_GPL=ON -DCMAKE_BUILD_TYPE=Release
    make -j$(nproc)
    sudo make install && sudo ldconfig
}

install_pyliblo()
{
    echo "INSTALL PYLIBLO"
    cd $HOME
    sudo pip3 install Cython --install-option="--no-cython-compile"
    wget 'http://das.nasophon.de/download/pyliblo-0.10.0.tar.gz'
    tar -zxvf 'pyliblo-0.10.0.tar.gz'
    cd 'pyliblo-0.10.0'
    sudo python3 ./setup.py build
    sudo python3 ./setup.py install
}

reboot_server()
{
    read -p "The server needs to reboot to complete installation. Do you want to reboot it now (y/n)?" choice
    case $choice in
        [Yy]* ) echo "Rebooting ..." 
                sudo reboot
                break
    esac
}

set -e
cd $HOME
update_sources
install_dev_tools
install_xorg
install_kernel_dev
install_python
install_lsb
install_awsclii
install_lightdm
install_nvidia
install_virtualgl
install_turbovnc
install_shmdata
install_splash
install_switcher
install_pyliblo
reboot_server
