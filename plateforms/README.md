Headless GPU server scripts
===========================

This directory regroups the scripts and documentation necessary to deploy and run a headless GPU server. A different directory must be created for each target platform, for example AWS, Azure, DigitalOcean, a physical server, etc.
