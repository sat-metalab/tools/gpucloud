#!/bin/bash

install_shmdata()
{
    echo "INSTALL SHMDATA"
    sudo apt install -y bash
    cd $HOME
    sudo apt install -y cmake bison build-essential flex libtool git
    sudo apt install -y libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev
    sudo apt install -y python3-dev
    git clone https://gitlab.com/sat-metalab/shmdata.git
    cd shmdata
    mkdir build && cd build
    cmake -DCMAKE_BUILD_TYPE=Release ..
    make
    sudo make install && sudo ldconfig
}

install_switcher()
{
    echo "INSTALL SWITCHER"
    cd $HOME
    DEBIAN_FRONTEND=noninteractive sudo apt install -y libxi-dev libglib2.0-dev libgstreamer-plugins-bad1.0-dev libjson-glib-dev libcgsi-gsoap-dev gstreamer1.0-libav gstreamer1.0-plugins-bad gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly liblo-dev linux-libc-dev libpulse-dev libportmidi-dev libjack-jackd2-dev jackd libvncserver-dev uuid-dev libssl-dev swh-plugins  libgl1-mesa-dev libglu1-mesa-dev freeglut3-dev mesa-common-dev libltc-dev libcurl4-gnutls-dev gsoap wah-plugins libxrandr-dev libxinerama-dev libxcursor-dev libsamplerate0-dev  libsoup2.4-dev python3-dev gcc-8 g++-8 libxxf86vm-dev
    git clone https://gitlab.com/sat-metalab/switcher.git
    cd switcher
    git checkout develop
    git submodule update --init --recursive
    mkdir build && cd build
    CC="gcc-8" CXX="g++-8" cmake .. -DENABLE_GPL=ON -DCMAKE_BUILD_TYPE=Release -DPLUGIN_GSOAP=OFF
    make -j"$(nproc)"
    sudo make install && sudo ldconfig
}

set -e
install_shmdata
install_switcher
