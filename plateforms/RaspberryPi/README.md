GPU cloud rendering using Raspberry Pi
======================================

## Prepare SD card
We need to download the ```Raspberry Pi OS (32-bit) with desktop and recommended software``` into the Raspberry Pi SD card, using the following steps.<br> <br>
Download and unzip the Raspberry Pi OS using the following commands:
```s
wget https://downloads.raspberrypi.org/raspios_full_armhf_latest
unzip raspios_full_armhf_latest 
```
As of this example, we obtain the image ```2020-05-27-raspios-buster-full-armhf.img```. The image's date may vary depending on when you download it. <br> <br>
Check your SD card path using:
```s
sudo fdisk -l
```
This command will display all the disk's partitions. The path to your SD card should be displayed last; however make sure you use the correct path as installing the Raspberry Pi OS image into the wrong path could corrupt the disk. <br> <br>
Write the image in your SD card using the following command:
```s
sudo dd if=2020-05-27-raspios-buster-full-armhf.img of=/dev/device_name bs=1M
``` 
Wait for the installation to be completed before inserting the SD card back into the Raspberry Pi and rebooting it. The Raspberry Pi OS Desktop should then be displayed.
## Configure Raspberry Pi
Clone this repository into the Rasberry Pi and run the script **configure_raspi.sh** using the command ```sh configure_raspi.sh```. It will download and install all necessary dependencies and modules including Switcher which, combined with the [shm_receiver.py](/module/README.md), will allow to receive and display Splash created shmdatas.