GPU cloud rendering using AWS
==============================

## Configure AWS server

Clone this repository into the AWS server and run the script **configure_aws_server.sh** using the command `sh ./configure_aws_server.sh`. It will download and install all necessary dependencies and modules, including VirtualVG and TurboVNC as well as Splash as a default software.

It will reboot the server to complete installation.


## Launch TurboVNC

On your server, run the command `sudo /opt/TurboVNC/bin/vncserver` to launch TurboVNC. It will demand a password on the first launch. The password should contain 8 characters.


## Connect to the VNC server

On your client, connect to the port 5901 of your AWS server where TurboVNC is running using the command `ssh -i ~/.ssh/*key_name*.pem -L 5901:127.0.0.1:5901 ubuntu@*ip_address*`

Run a VNC viewer and choose *localhost:1* as the VNC server. Enter the password you entered during the first launch.

Run Splash by using the following command `vglrun splash`.
