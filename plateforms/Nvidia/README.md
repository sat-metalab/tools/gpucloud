GPU rendering using an Nvidia machine
=====================================

NB : This is a Work In Progress and the Dockerfile doesn't work yet.

## Download Docker and NVIDIA-Docker
It is necessary to Nvidia-Docker to use the Nvidia runtime to run your Docker image and for that Nvidia-Docker and Docker-ce need have to be in sync. This may mean that you would have to uninstall the Docker version you currently have on your machine.

### Set up the nvidia-docker2 repository
Add the repo key: <br><br>
``` curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add - ```

### Configure the needed repos
Create the file ```nvidia-docker``` file using: <br><br>
``` sudo touch /etc/apt/sources.list.d/nvidia-docker.list ``` <br><br>
Add the following lines to it: 
```s
deb https://nvidia.github.io/libnvidia-container/ubuntu18.04/$(ARCH) /
deb https://nvidia.github.io/nvidia-container-runtime/ubuntu18.04/$(ARCH) /
deb https://nvidia.github.io/nvidia-docker/ubuntu18.04/$(ARCH) / 
```
Save the file and run ```sudo apt-get update``` to refresh the package list.

### Check the installable versions of nvidia-docker2
Run the following command: <br><br>
```apt-cache madison nvidia-docker2​​​​​​​```. <br><br>
This will display multiple installable versions of nvidia-docker2. Keep in mind the most recent version. For the rest of this documentation, this version is ```2.0.3+docker18.09.7-3```.

### Install Docker
Install the required packages using the command: <br><br>
```sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common```. <br><br>
Add the docker key using the command: <br><br>
```sudo apt-key fingerprint 0EBFCD88 ```. <br><br>
Add the repository for bionic: <br><br>
```sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"```. <br><br>
This keeps nvidia-docker2 from failing during its installation. <br><br>
Check the installable docker versions using the command ```apt-cache madison docker-ce```. This will display multiple installable versions but choose the most recent one compatible with the nvidia-docker2 versions chosen above. In our case, that version is ```5:18.09.7~3-0~ubuntu-bionic```. <br><br>
You can then install docker using the specific chosen version: <br><br>
```sudo apt-get install docker-ce=5:18.09.7~3-0~ubuntu-bionic docker-ce-cli=5:18.09.7~3-0~ubuntu-bionic containerd.io```.

### Install nvidia-docker2
Install nvidia-docker2 using the version chosen above as well as the compatible nvidia runtime: <br><br>
```sudo apt-get install nvidia-docker2=2.0.3+docker18.09.7-3 nvidia-container-runtime=2.0.0+docker18.09.7-3 ```. <br><br>
Restart docker to enable the configuration of the nvidia-docker2 runtime: <br><br>
```sudo systemctl restart docker.service```.

# Build the docker image
Clone this git repository into your server and run the docker image using the following command: <br><br>
```sudo docker build . -t turbovnc```

# Run the container
Run the container by using : <br><br>
```sudo docker run -d --init --runtime=nvidia -ti -v /tmp/.X11-unix/X0:/tmp/.X11-unix/X0 -p 5901:5901 turbovnc /bin/bash```. <br><br>
This will open up a shell terminal in which you can run  ```sudo /opt/TurboVNC/bin/vncserver``` to launch the VNC Server and choose the password. It should contain 8 characters.

## Connect to the VNC server
On your client, connect to the port 5901 of your Azure server where TurboVNC is running using the command ``` ssh -L 5901:127.0.0.1:5901 *account_name@*ip_address* ``` <br><br>
Run a VNC viewer and choose *localhost:1* as the VNC server. Enter the password you entered during the first launch. <br><br>
Run Splash by using the following command ```vglrun splash```.