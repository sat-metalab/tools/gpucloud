# Class for facilitating the sending of shmdatas through Switcher

import time
import pyquid

from os import path
from pyshmdata import Reader
from typing import Any, Dict, List

class ShmdataStatistics():
    def __init__(self) -> None:
        self._bandwidth  : float = 0
        self._buffer_size: int = 0
        self._framerate : float = 0
        self._init_time : time = time.time()
        self._last_frame_time : time = time.time()


class ShmCloud():
    def __init__(self, is_caller: bool) -> None:
        """
        Constructor of ShmCloud

        :param is_caller:   specified whether the script creating this call is going to be the caller/sender
                            or the receiver of the shmdatas
        """

        self._shmdatas: Dict[str, Reader] = {}  #association between the shmdata's path and its assigned Reader.
        self._peers: List[str] = []  # list of distant hosts
        self._caps: Dict[str, str] = {}  # association between a shmdata and its datatype
        self._bandwidths: Dict[str, int] = {}  # association between a shmdata and its bandwidth
        self._statistics: Dict[str, ShmdataStatistics] = {}  # association between a shmdata and its statistics
        if is_caller :
            self._sw_qrox : pyquid.Qrox = pyquid.Switcher('sipcall', debug = True)
            self._sip_quiddity = self._sw_qrox.create('sip', 'caller').quid()
            self._sip_quiddity.set('port', 15075)
            self._audio_quids : List[str] = [] # list of audio quiddities
        else :
            self._sw_qrox : pyquid.Qrox = pyquid.Switcher('sipreceiv', debug = True)
            self._sip_quiddity = self._sw_qrox.create('sip', 'receiver').quid()
            self._sip_quiddity.set('port', 25060)

    def add_peer(self, contact: str) -> None:
        """
        Connects to a distant host

        :param contact: distant's host contact information as extracted from the contact book
        :return: bool. True for success, false otherwise
        """
        self._sip_quiddity.invoke('add_buddy', [contact + ':5060'])
        self._sip_quiddity.invoke('authorize', [contact + ':5060', True])

    def attach_dummy_to_peer(self, contact: str) -> str:
        """
        Attaches an empty audio shmdata to be sent to a certain distant host.
        :param contact: distant host's contact information as extracted from the contact book
        :return str : path of the dummy shmdata created
        """
        dummy_quid = self._sw_qrox.create('audiotestsrc').quid()
        dummy_quid.set('started', True)
        dummy_shmpath = dummy_quid.make_shmpath('audio')

        return self.attach_shmdata_to_peer(dummy_shmpath, contact)

    def attach_video_to_peer(self, shmdata_path: str, contact: str) -> str:
        """
        Prepares a shmdata to be sent to a certain distant host. This is meant to be used by the caller.

        :param shmdata_path: path to shmdata
        :param contact: distant's host contact information as extracted from the contact book
        :return str: path of the encoded video shmdata created
        """

        reader = Reader(path=shmdata_path, callback=self._reader_callback, user_data=shmdata_path)
        self._shmdatas[shmdata_path] = reader
       
        if self._sw_qrox.class_doc('nvenc') != "null":
            converted_video_shmpath = self.convert_video(shmdata_path)
            shmpath = self.encode_shmdata('nvenc', converted_video_shmpath)
        else:
            shmpath = self.encode_shmdata('videnc', shmdata_path)

        return self._attach_shmdata_to_peer(shmpath, contact)

    def convert_video(self, shmdata_path: str) -> str:
        """
        Converts video.

        :param shmdata_path: path to shmdata
        :return str : shmdata path of the converted video
        """

        video_convert_quid = self._sw_qrox.create('videoconvert').quid()
        video_convert_quid.invoke('connect', [shmdata_path])
        video_convert_shmpath = video_convert_quid.make_shmpath('video')


        if self._wait_for_shmdata(video_convert_shmpath):
            return video_convert_shmpath
        else :
            return str()

    def connect_audio_to_jack(self, shmdata_path: str) -> None:
        """
        Connects an audio shmdata to jack
        :param shmdata_path : path to shmdata
        """

        jacksink_quid = self._sw_qrox.create('jacksink').quid()
        jacksink_quid.set('auto_connect', True)
        jacksink_quid.invoke('connect', [shmdata_path])

    def connect_oscsrc_to_sink(self, shmdata_path: str) -> None:
        """
        Connects a OSCsrc received from the caller to a OSCsink
        :param shmdata_path: path to shmdata
        """
        self._oscsink_quid.invoke('connect', [shmdata_path])

    def create_audio_shmdata(self, audio_path: str) -> str:
        """
        Creates an audio shmdata from a given audio file.
        :param audio_path: path to audio file
        :return str: path of the audio shmdata created
        """
        audiosrc_quid = self._sw_qrox.create('filesrc').quid()
        audiosrc_quid.set('location', audio_path)
        audiosrc_quid.set('loop', True)
        audiosrc_quid.set('play', False)
        audiosrc_shmpath = audiosrc_quid.make_shmpath('audio')

        self._audio_quids.append(audiosrc_quid)
        
        return audiosrc_shmpath

    def create_ltc_shmdata(self, audio_shmdata: str) -> str:
        """
        Creates a timecode shmdata from a given audio shmdata.
        :param shmdata_path: path to shmdata
        :return str: path of the timecode shmdata
        """
        ltc_quid = self._sw_qrox.create('ltcsource').quid()
        ltc_quid.invoke('connect', [audio_shmdata])
        ltc_quid.set('time_reference', 1) #relative timecode from start time
        ltc_quid.set('started', True)
        ltc_shmpath = ltc_quid.make_shmpath('audio')

        return ltc_shmpath

    def create_switcher_window(self, shmdata_path: str) -> None:
        """
        Opens a shmdata in a glf window.

        :param shmdata_path: path to shmdata
        """

        win_qrox = self._sw_qrox.create('glfwin')
        win_quid = win_qrox.quid()
        win_quid.invoke('connect', [shmdata_path])

    def detach_shmdata_from_peer(self, shmdata_path: str, contact: str) -> None:
        """
        Removes a shmdata from the list of shmdatas to be sent to the distant host. This is meant to be used by the caller.

        :param shmdata_path: path to shmdata
        :param contact: distant's host contact information as extracted from the contact book
        """
        self._sip_quiddity.invoke('attach_shmdata_to_contact', [shmdata_path, contact + ':5060', False])
        self._shmdatas.pop(shmdata_path, None)

    def encode_shmdata(self, encoding: str, shmdata_path: str) -> str:
        """
        Encodes a video shmdata with a specified encoding

        :param encoding: encoding name
        :param shmdata_path: path to shmdata
        :return str: path to the encoded shmdata
        """

        enc_quid = self._sw_qrox.create(encoding).quid()
        enc_quid.invoke('connect', [shmdata_path])
        enc_path = enc_quid.make_shmpath('video-encoded')

        if self._wait_for_shmdata(enc_path):
            return enc_path
        else:
            return str()

    def hang_up_on_peer(self, contact: str) -> None:
        """
        Stops all reception from / sending to a certain contact.

        :param contact: distant's host contact information as extracted from the contact book
        """
        self._sip_quiddity.invoke('hang-up', [contact + ':5060'])
    
    def register_to_server(self, address: str, password: str) -> None:
        """
        Register the server

        :param address: the server's user name
        """
        split_uri = address.split('@')
        contact_username = split_uri[0]
        domain = split_uri[1]

        self._sip_quiddity.invoke('register', [address + ':5060', password])
        self._sip_quiddity.invoke('set_stun_turn', [domain + ':3478',
                                              domain + ':3478', contact_username, password])
        self._sip_quiddity.invoke('mode', 'authorized contacts')

    def remove_peer(self, contact: str) -> None:
        """
        Remove a contact from the server's contact list

        :param contact: distant's host contact information as extracted from the contact book
        """
        self._sip_quiddity.invoke('del_buddy', [contact + ':5060'])

    def send_message_via_osc(self, path: str, message) -> None:
        """
        Sends a message via OSC
        :param path: path to which send a message
        :param message: message to send
        """
        liblo.send(liblo.Address('localhost', 15008), path, message)

    def send_shmdatas_to_peer(self, contact: str) -> None:
        """
        Sends all attached shmdatas of a contact to the distant host

        :param address: distant's host contact information as extracted from the contact book
        """
        
        self._sip_quiddity.invoke('send', [contact + ':5060'])

    def statistics(self) -> Dict[str, ShmdataStatistics]:
        """
        Returns all relevant statistics of all the connected shmdatas.

        :return Dict[str,ShmdataStatistics]:
            key:  path to shmdata
            value: relevant statistics concerning each shmdata
        """
        return self._statistics

    def play_audio_files(self) -> None:
        """
        Starts to play all audio files sent
        """

        for audio_quid in self._audio_quids:
            audio_quid.set('play', True)

    def attach_shmdata_to_peer(self, shmdata_path: str, contact: str) -> str:
        if self._wait_for_shmdata(shmdata_path):
            self._sip_quiddity.invoke('attach_shmdata_to_contact', [shmdata_path, contact + ':5060', True])
            return shmdata_path
        else:
            return str()

    def _reader_callback(self, user_data, buffer, datatype: str, parsed_datatype: Dict[str, Any]) -> None:
        ## Update _statistics
        if user_data not in self._statistics:
            self._statistics[user_data] = ShmdataStatistics()

        shmdata_statistics = self._statistics.get(user_data)

        shmdata_statistics._buffer_size = len(buffer)

        callback_time = time.time()

        total_size = shmdata_statistics._bandwidth * ( shmdata_statistics._last_frame_time - shmdata_statistics._init_time)
        total_frames = shmdata_statistics._framerate * ( shmdata_statistics._last_frame_time - shmdata_statistics._init_time)

        if callback_time != shmdata_statistics._init_time:
            shmdata_statistics._bandwidth = (total_size + len(buffer)) / (callback_time - shmdata_statistics._init_time)
            shmdata_statistics._framerate = (total_frames + 1) / (callback_time - shmdata_statistics._init_time)

        shmdata_statistics._last_frame_time = callback_time

        self._statistics[user_data] = shmdata_statistics

        ## Update _caps
        if user_data not in self._caps:
            self._caps[user_data] = datatype

    def _wait_for_shmdata(self, shmdata_path: str) -> bool:
        timeout = time.time() + 5 # 5 secondes before timeout
      
        while not path.exists(shmdata_path) and time.time() < timeout:
            pass

        is_success : bool = time.time() < timeout
        return is_success
