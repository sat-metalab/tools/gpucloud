#!/usr/bin/env python3

import glob
import logging
import time

from argparse import ArgumentParser
from shmcloud.shmcloud import ShmCloud

logger = logging.getLogger(__name__)

parser = ArgumentParser(description="shmdata receiver")
parser.add_argument("-a", "--address", help="SIP account to connect as")
parser.add_argument("-p", "--password", help="SIP account password")
parser.add_argument("-c", "--contacts", nargs='+', help="Add a contact to receive shmdatas from. This can be a space-separated list to specify multiple contacts.")
parser.add_argument("-w", "--window", action='store_true', help="Open a glfwin when a shmata is received.")

args = parser.parse_args()

if __name__ == "__main__":
    address = args.address
    password = args.password
    contacts = args.contacts
    show_window = args.window

    if not address:
        logger.warning("Specify an account to connect as")
        quit()

    if not password:
        logger.warning("A password is needed")
        quit()

    if not contacts:
        logger.warning("Specify at least one contact")
        quit()

    cloud_receiver = ShmCloud(False)
    cloud_receiver.register_to_server(address, password)

    for contact in contacts:
        cloud_receiver.add_peer(contact)
    
    list_of_shmdatas = [f for f in glob.glob("/tmp/switcher_*")]

    received_shmdatas : bool = False

    while True:
         try:
             new_list_of_shmdatas = [f for f in glob.glob("/tmp/switcher_*") if f not in list_of_shmdatas]
             for new_shmdata in new_list_of_shmdatas:
                 if 'rtp' not in new_shmdata:
                     if not received_shmdatas :
                         for contact in contacts:
                             dummy_shmdata = cloud_receiver.attach_dummy_to_peer(contact)
                             cloud_receiver.send_shmdatas_to_peer(contact)
                             received_shmdatas = True
                             list_of_shmdatas.append(dummy_shmdata)
                     elif 'ltcsource' in new_shmdata:
                        pass
                     elif new_shmdata.endswith('audio'):
                        cloud_receiver.connect_audio_to_jack(new_shmdata)
                     elif show_window and new_shmdata.endswith('video'):
                        cloud_receiver.create_switcher_window(new_shmdata)

                 list_of_shmdatas.append(new_shmdata)
         except KeyboardInterrupt:
             quit()
