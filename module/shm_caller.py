#!/usr/bin/env python3

import glob
import logging
import time

from argparse import ArgumentParser
from os import path
from shmcloud.shmcloud import ShmCloud

logger = logging.getLogger(__name__)

parser = ArgumentParser(description="shmdata receiver")
parser.add_argument("-a", "--address", help="SIP account to connect as")
parser.add_argument("-p", "--password", help="SIP account password")
parser.add_argument("-c", "--contacts", nargs='+',
                    help="Add a contact to send shmdatas to. This can be a space-separated list to specify multiple contacts.")
parser.add_argument("-s", "--shmdatas", nargs='+',
                    help="Specify shmdatas to send. All shmdatas will be sent to all contacts")
parser.add_argument("-f", "--audio_files", nargs='+',
                    help="Specify audio files to send. All audio files will be sent to all contacts")

args = parser.parse_args()

if __name__ == "__main__":
    address = args.address
    password = args.password
    contacts = args.contacts
    shmdatas = args.shmdatas or []
    audio_files = args.audio_files or []

    if not address:
        logger.warning("Specify an account to connect as")
        quit()

    if not password:
        logger.warning("A password is needed")
        quit()

    if not contacts:
        logger.warning("Specify at least one contact to send shmdatas to")
        quit()

    cloud_caller = ShmCloud(True)
    cloud_caller.register_to_server(address, password)

    audio_shmpaths = []
    ltc_shmpaths = []

    for audio_file in audio_files:
        if path.exists(audio_file):
            audio_shmpath = cloud_caller.create_audio_shmdata(audio_file)
            audio_shmpaths.append(audio_shmpath)
            ltc_shmpath = cloud_caller.create_ltc_shmdata(audio_shmpath)
            ltc_shmpaths.append(ltc_shmpath)
            cloud_caller.connect_audio_to_jack(ltc_shmpath)
        else:
            logger.warning('Path ' + audio_file + ' does not exist. Skipping.')

    for contact in contacts:
        cloud_caller.add_peer(contact)
        cloud_caller.attach_dummy_to_peer(contact)
        for shmdata in shmdatas:
            if path.exists(shmdata):
                cloud_caller.attach_video_to_peer(shmdata, contact)
            else:
                logger.warning('Path ' + shmdata + ' does not exist. Skipping.')

        for audio_shmpath in audio_shmpaths:
            cloud_caller.attach_shmdata_to_peer(audio_shmpath, contact)

        for ltc_shmpath in ltc_shmpaths:
            cloud_caller.attach_shmdata_to_peer(ltc_shmpath, contact)

        cloud_caller.send_shmdatas_to_peer(contact)

        list_of_shmdatas = [f for f in glob.glob("/tmp/switcher_*")]

        received_dummy: bool = False

        # Note the following (detection of a new RTP shmdata) can be improved
        # using switcher notification of new shmdata. This could be done with
        # registering a callback to the infotree of the sip quiddity. Once an
        # RTP shmdata writer is created by any quiddity, it is specified and
        # notified throught the infotree. An exemple of registering to the
        # infotree is here:
        # https://gitlab.com/sat-metalab/switcher/-/blob/master/wrappers/python/examples/04-signal.py
        while not received_dummy:
            try:
                new_list_of_shmdatas = [f for f in glob.glob(
                    "/tmp/switcher_*") if f not in list_of_shmdatas]
                for new_shmdata in new_list_of_shmdatas:
                    if 'rtp' not in new_shmdata:
                        received_dummy = True
                    list_of_shmdatas.append(new_shmdata)
            except KeyboardInterrupt:
                quit()

    cloud_caller.play_audio_files()

    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            quit()
