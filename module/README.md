shmcloud
========

**shmcloud** is a Python module whose goal is to embed Switcher's capability to send a shmdata over the network to a distant computer, in a simpler package. As such **shmcloud** does not aim to be feature-complete, it mostly takes a contact list and allows for sending each one of them some shmdatas, either generated internally by Switcher or from an external software.


## Tools

A few tools are bundled with **shmcloud**, which make use of it and make it easier to test its functionalities. For now there are:

* shm_caller.py, which allows for calling contacts and sending shmdatas to them
* shm_receiver.py, which allows for receiving the aforementioned shmdatas

An example usage of these tools would be as follows. First on the receiving side:

```bash
./shm_receiver.py -a receiver@some.sip.server.com -p somepwd -c caller@some.other.sip.server.com
```

Adding the option ```-w``` or ```--window``` will allow the shmdatas received to be displayed in glf windows.

Then on the sending side:

```bash
./shm_caller.py -a caller@some.other.sip.server.com -p otherpwd -c receiver@some.sip.server.com -s /path/to/shmdata -f /path/to/audio/file
```
Make sure to have the Jack server opened using the command ```sudo jackd --no-realtime -d dummy -r 48000 -p 1024```.


If all goes well, the connection should be initiated and you should see the frames flowing from one side to the other.
