# To launch Splash with the Python script run splash with the -P or --python options:
# splash -P name_of_script.py splash_config.json -- scripts_argument
# In our case:
# splash -P splash2switcher.py splash_config.json

import os
import sys

# add paths to local module shmcloud
path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(1, os.path.realpath(os.path.join(path, '../shmcloud')))

import json
import shmcloud
import splash

from typing import Any, Dict, Union

"""
For reference, 'sink_shmdata' default attributes:
 {
     'caps': ['application/x-raw'],
     'framerate': [30],
     'opened': [0],
     'bufferCount': [3],
     'timestamp': [0],
     'socket': ['/tmp/splash_sink'],
     'priorityShift': [0],
     'savable': [1],
     'alias': ['sink_shmdata_1']
 }

'sink_shmdata_encoded' default attributes:
{
    'caps': [''],
    'codecOptions': ['profile=baseline'],
    'bitrate': [4000000],
    'framerate': [30],
    'codec': ['h264'],
    'opened': [0],
    'bufferCount': [3],
    'timestamp': [0],
    'socket': ['/tmp/splash_sink'],
    'priorityShift': [0],
    'savable': [1],
    'alias': ['sink_shmdata_encoded_1']
}
"""

address: str = ''
password: str = ''
contact: str = ''
sink_names: Dict[str, bool]= {}
sink_types: Dict[str, str] = {}
sink_shmpaths: Dict[str, str] = {}
cloud_caller = shmcloud.ShmCloud(is_caller=True)

def parse_args() -> Dict[str, Any]:
    # if the script receives arguments, it is stored in sys.argv
    args = {}
    if len(sys.argv) > 1: # the first element of the list is always the script path
        # we have arguments to parse
        args["config"] = sys.argv[1] # path to the config file
    return args


def pre_process(value: Union[float, int]) -> bool:
    """
    Pre-process the value returned from
    splash.get_object_attribute(sink, 'opened')[0]
    """
    int_value = int(abs(value))
    return  False if int_value == 0 else True


def update_sinks_by_type(sink_type: str) -> None:
    """
    Verify the current states of sinks
    """
    global sink_names
    global sink_types
    global cloud_caller

    for sink in splash.get_objects_of_type(sink_type):
        if sink not in sink_names:
            sink_names[sink] = pre_process(splash.get_object_attribute(sink, 'opened')[0])
            sink_types[sink] = sink_type
            sink_shmpaths[sink] = splash.get_object_attribute(sink, 'socket')[0]

            if sink_names[sink]:
                cloud_caller.attach_shmdata_to_peer(shmdata_path=sink_shmpaths[sink], contact=contact)
        else:
            current_value = pre_process(splash.get_object_attribute(sink, 'opened')[0])
            if not current_value and sink_names[sink]:
                sink_names[sink] = False
                cloud_caller.detach_shmdata_from_peer(shmdata_path=sink_shmpaths[sink], contact=contact)

            elif current_value and not sink_names[sink]:
                sink_names[sink] = True
                cloud_caller.attach_shmdata_to_peer(shmdata_path=sink_shmpaths[sink], contact=contact)

    for sink in sink_names:
        if sink not in splash.get_objects_of_type(sink_type) and sink_types[sink] == sink_type:
            cloud_caller.detach_shmdata_from_peer(shmdata_path=sink_shmpaths[sink], contact=contact)
            # we do not remove the sink from the list
            sink_names[sink] = False


def load_config(path: str) -> None:
    global address
    global password
    global contact

    with open(path) as f:
        data = json.load(f)
        # set the variables from the config
        address = data['address']
        password = data['password']
        contact = data['contact']


def set_splash_configuration() -> None:
    splash.add_custom_attribute('address')
    splash.add_custom_attribute('password')
    splash.add_custom_attribute('contact')


def splash_init() -> None:
    global cloud_caller
    # First, change the loopRate of the python script
    splash.set_object_attribute(splash.get_interpreter_name(), 'loopRate', 1)
    args = parse_args()
    load_config(args['config'] if 'config' in args else './config/default.json')
    set_splash_configuration()
    cloud_caller.register_to_server(address=address, password=password)
    cloud_caller.add_peer(contact=contact)



def splash_loop() -> bool:
    global contact

    # check regularly for new or deleted Sink objects
    update_sinks_by_type('sink_shmdata_encoded')
    update_sinks_by_type('sink_shmdata')
    cloud_caller.send_shmdatas_to_peer(contact=contact)
    return True

def splash_stop() -> None:
    # make sure that all the sinks are disconnected
    global contact
    global sink_names
    global sink_shmpaths

    # disconnect the remaining active sinks
    for sink in sink_names:
        if sink_names[sink]:
            splash.set_attribute(sink, 'opened', False)
            cloud_caller.detach_shmdata_from_peer(shmdata_path=sink_shmpaths[sink], contact=contact)

    cloud_caller.hang_up_on_peer(contact=contact)
    cloud_caller.remove_peer(contact=contact)
