VirtualGL basics
================

## VirtualGL's objective
VirtualGL is an open source program that makes it possible to run OpenGL applications with 3D hardware acceleration on any VNC or other Unix and Linux client environments.

With VirtualGL, the OpenGL commands and 3D data are instead redirected to a GPU in the application server, and only the rendered frames are sent over the network.

We can find multiple problems with the traditional ways to run OpenGL applications:

* Some client environments don't support OpenGL applications at all.
* Some client environments will force the OpenGL applications to run without the benefit of OpenGL hardware acceleration.
* Displaying 3D applications with hardware acceleration, on the other hand, requires the use of "indirect rendering". Simply put, indirect rendering happens when a remote OpenGL application will be rendered on a local desktop. All OpenGL commands and graphic data are sent over the network to be rendered over the X11 protocol thus shipped from an application to an X Display. However, this process is only effective when the 3D data is small and static and the network is fast. It also requires the OpenGL applications to be specifically tuned for a remote X Server environment, which isn't always the case.

VirtualGL aims to solve all the presented problems. Instead of using the machine's capacities, all graphic data is instead redirected to the GPU of the application's server and only the rendered frames are sent over network. This allows to have relatively thin clients and increases the amount of data the users can visualize in real time. Plus, it doesn't require any particular configuration from the application. Any OpenGL application should work the same way it did before without any change.

You can find out more on the [VirtualVG official website](https://virtualgl.org/About/Introduction).

## How it works
VirtualGL uses a technique called "split rendering". It intercepts all OpenGL commands and 3D data and redirects them to the GPU attached to the application's server - called the *3D X Server* - instead of being passed over the X11 protocol to be rendered in the local X Server. Then, when a frame is rendered, VirtualGL transports it back to the local X Server, called the *2D X Server* to be displayed on the application's window as it's now considered a 2D drawing command.

## Example
Let's illustrate this using our implementation of a GPU cloud rendering with AWS which you can find in *plateforms/AWS*. In this example, we created a GPU instance on AWS then installed LightDM as a display manager. As LightDM is enabled, it will start the X Server - more specifically, what will be considered as the *3D X Server*. This, combined with the installation of VirtualGL, will enable the use of the AWS' GPU instance for all OpenGL commands and 3D data. In this example, we will use a VNC (Virtual Network Computing) to connect to LightDM. More specifically, we use TurboVNC.

TurboVNC allows VirtualGL to remotely display 3D applications. It possesses a high speed virtual X Server designed for use with 3D applications offering high performances, especially compared to its parent - TightVNC. Thus, TurboVNC's X Server is our *2D X Server*.

The GPU cloud rendering with Microsoft Azure, which you can find in *plateforms/Azure*, works following this exact same logic.

## TurboVNC's role
TurboVNC is a VNC (Virtual Network Computing) that allows VirtualGL to remotely display 3D applications. It is a high speed virtual X Server designed for use with 3D applications offering high performances, especially compared to its parent - TightVNC.
